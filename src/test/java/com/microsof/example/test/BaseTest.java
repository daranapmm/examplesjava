package com.microsof.example.test;

import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected WebDriver driver;

    @BeforeTest
    public void setupDriver(){

        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver");

        this.driver = new ChromeDriver();
    }


    @AfterTest
    public void quitDriver(){
        driver.quit();
    }



}
